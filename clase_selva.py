import pygame

class Animal:
    nombre=None     
    genero=None
    edad=None   
    color=None
    peso=None
    tipo=None
    _codigo=None

    def come(self):
        print(self.nombre," come")
    
    def duerme(self):
        print(self.nombre," duerme")

    def caza(self):
        print(self.nombre," caza")
        
class Leon(Animal):
    cabellera=None
    def __init__(self,nombre,genero,edad,color,tipo=None,peso=None,codigo=None):
        self.nombre=nombre
        self.genero=genero
        self.edad=edad
        self.color=color
        self.tipo=tipo
        self.peso=peso
        self._codigo=codigo

    def ruge(self):
        pygame.init()
        pygame.mixer.init()
        pygame.mixer.music.load('sonidos\\leon.wav')
        pygame.mixer.music.play()
