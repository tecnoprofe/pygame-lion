import pygame
class Calabaza(pygame.sprite.Sprite): 
           
    def __init__(self):     #Constructor
        super(Calabaza, self).__init__()
 
        self.images = []
        self.images.append(pygame.image.load('sprite/calabaza/walk1.png'))
        self.images.append(pygame.image.load('sprite/calabaza/walk2.png'))
        self.images.append(pygame.image.load('sprite/calabaza/walk3.png'))
        self.images.append(pygame.image.load('sprite/calabaza/walk4.png'))
        self.images.append(pygame.image.load('sprite/calabaza/walk5.png'))
        self.images.append(pygame.image.load('sprite/calabaza/walk6.png'))
        self.images.append(pygame.image.load('sprite/calabaza/walk7.png'))
        self.images.append(pygame.image.load('sprite/calabaza/walk8.png'))
        self.images.append(pygame.image.load('sprite/calabaza/walk9.png'))
        self.images.append(pygame.image.load('sprite/calabaza/walk10.png'))
 
        self.index = 0
        self.X = 200 
        self.Y = 100 
        self.image = self.images[self.index]
        self.vida=10
 
        self.rect = pygame.Rect(self.X, self.Y, 350, 498)
 
    def update(self):        
        self.index += 1 
        if self.index >= len(self.images):
            self.index = 0        
        self.image = self.images[self.index]

    def caminar(self):
        self.rect.x += 5

    def retroceder(self):
        self.rect.x -= 5

    def saltar(self):
        self.rect.y = self.Y  - 50

    def bajar(self):
        self.rect.y = self.Y  + 50

    def incrementarvida(self):
        self.vida+=1        
        
