# Mini-invaders, version 0.06
# (Comprobac. de colisiones)
# Parte de la intro a Pygame, por Nacho Cabanes
 
import pygame
from pygame.locals import *
 
pygame.init()
 
ancho = 800
alto = 600
velocidadX = 3
velocidadY = 3
terminado = False
 
pantalla = pygame.display.set_mode( (ancho, alto) )
pygame.key.set_repeat(1,25)
reloj = pygame.time.Clock()
 
imagenMarciano = pygame.image.load("sprite/calabaza/walk1.png")
rectanguloMarciano = imagenMarciano.get_rect()
rectanguloMarciano.left = 0
rectanguloMarciano.top = 0
 
imagenNave = pygame.image.load("sprite/dinosaurio/Dead (1).png")
rectanguloNave = imagenNave.get_rect()
print(rectanguloNave)
rectanguloNave.left = 0
rectanguloNave.top = 0


 
pygame.font.init() # Iniciar textos                            
font = pygame.font.SysFont(None, 60) 

while not terminado:
    pantalla.fill((255, 255, 255))  
    
    # ---- Comprobar acciones del usuario ----
    for event in pygame.event.get():
        if event.type == pygame.QUIT: terminado = True
    
    
    keys = pygame.key.get_pressed()
    if keys[K_LEFT]: 
        print("NO choque")       
        rectanguloNave.left-=3       
    if keys[K_RIGHT]:      
        print("NO choque")       
        rectanguloNave.left += 3    
        print(rectanguloNave.left)
        
    
    

    if not rectanguloNave.colliderect(rectanguloMarciano ):
        txtgameover = font.render("GAME OVER", True, (0,0,255))        
        pantalla.blit(txtgameover, (15, 5))
                                           
 
    # ---- Dibujar elementos en pantalla ----
    
    pantalla.blit(imagenNave, (rectanguloNave.left, 0))
    pantalla.blit(imagenMarciano, (600,0))
    
    pygame.display.flip()
       
    # ---- Ralentizar hasta 40 fotogramas por segundo  ----
    reloj.tick(24)
 
# ---- Final de partida ----
pygame.quit()